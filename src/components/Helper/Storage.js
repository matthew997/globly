import AsyncStorage from '@react-native-community/async-storage';
function removeAsyncStorage(key) {
    AsyncStorage.removeItem(key);
}
async function storeData(key, data) {
    try {
        // await AsyncStorage.removeItem(key);
        await AsyncStorage.setItem(key, data)
    
    } catch (e) {
        // saving error
    }
}

async function getData(key) {
    var r;
    try {
        return await AsyncStorage.getItem(key);
    } catch (error) {
        console.log(error.message);
    }
}
export { removeAsyncStorage, storeData, getData };
