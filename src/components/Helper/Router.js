import React from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { Image, Dimensions } from 'react-native';

// import MyList from '../../screens/MyListView';
import MainListView from '../screens/MainListView';
import PostsPage from '../screens/PostsPage';
import Favourite from '../screens/Favourite';


import { CustomDrawer } from './CustomDrawer';
var width = Dimensions.get('window').width;
const TabScreen = createBottomTabNavigator(
  {
    QuestionsList: {
      screen: PostsPage,
      navigationOptions: {
        tabBarLabel: "Główna",
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require("../../images/home.png")}
            resizeMode="contain"
            style={{ width: 20, height: 20, tintColor: '#727272' }}
          />
        )
      }
    },
    MyList: {
      screen: MainListView,
      navigationOptions: {
        tabBarLabel: "Szukaj",
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={require("../../images/search.png")}
            resizeMode="contain"
            style={{ width: 20, height: 20, tintColor: '#727272' }}
          />
        )
      }
    },
    Profile: {
      screen: MainListView,
      navigationOptions: {
        tabBarLabel: "Ulubione",
        tabBarIcon: ({ tintColor }) => (
          <Image
          tintColor='#727272'
            source={require("../../images/like.png")}
            resizeMode="contain"
            style={{ width: 20, height: 20,  }}
          />
        )
      }
    },
  },

  {
    labelPosition: 'top',
    swipeEnabled: false,
    animationEnabled: false,
    tabBarOptions: {
      // activeTintColor: '#727272',
      // inactiveTintColor: '#727272',
      style: {
        backgroundColor: '#1E96FC', //tło bottomNav
        height: 45,
        marginTop: -10,
      },
      labelStyle: {
        textAlign: 'center',
        textTransform: 'lowercase',
      },
      indicatorStyle: {
        borderBottomColor: '#727272',
        borderBottomWidth: 4,
      },
    },
  }


);

const DrawerNavigator = createDrawerNavigator({
  // Home:{screen:TabScreen,
  //   navigationOptions: {
  //     drawerIcon: ({ tintColor }) => (
  //       <Image
  //         source={require("../../images/plus.png")}
  //         resizeMode="contain"
  //         style={{ width: 20, height: 20, tintColor:'white'}}
  //       />
  //     )
  //   }
  // },
  Home: {
    screen: PostsPage,
    navigationOptions: {
      title: 'Główna',
      drawerIcon: ({ tintColor }) => (
        <Image
          source={require("../../images/home-icon.png")}
          resizeMode="contain"
          style={{ width: 20, height: 20, tintColor: 'white' }}
        />
      )
    }
  },
  About: {
    screen: Favourite,
    navigationOptions: {
      title: 'Ulubione',
      drawerIcon: ({ tintColor }) => (
        <Image
          source={require("../../images/favorite-heart-button.png")}
          resizeMode="contain"
          style={{ width: 20, height: 20, tintColor: 'white' }}
        />
      ),

    }
  },

}, {
  drawerWidth: width / 2 + 50,
  drawerPosition: "right",
  contentComponent: props => <CustomDrawer {...props} />
});
export { DrawerNavigator };
