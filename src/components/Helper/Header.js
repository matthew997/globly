import React, {Component} from 'react';
import {Image, TouchableOpacity, ScrollView, SafeAreaView,DrawerItems} from 'react-native';

class LogoTitle extends Component{
    render(){
        return(
        <Image
            source={require('../../images/logo.png')}
            style={{ width: 45, height: 45,marginLeft:25, alignSelf:'center' }}
        />
        );
    }
}
class MenuIcon extends Component{
  render(){
    navigation = this.props.navig;
      return(
        <TouchableOpacity onPress={()=>navigation.toggleDrawer()}>
          <Image
              style={{ width: 30, height: 30, marginRight:25 }}
            source={require('../../images/menu.png')}/>
        </TouchableOpacity>
      );
  }
}



const CustomDrawerContentComponent = props => (
    <ScrollView style={{flex:1}}>
      <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
        <DrawerItems {...props} style={{align:'right'}}/>
      </SafeAreaView>
    </ScrollView>
  );


export {LogoTitle, MenuIcon, CustomDrawerContentComponent};
