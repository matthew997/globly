import React, { Component } from 'react';
import { DrawerItems } from 'react-navigation-drawer';
import { getData } from '../Helper/Storage';
import {
  ScrollView,
  SafeAreaView,
  View, Text,
  Image, TouchableOpacity,
  StyleSheet
} from 'react-native';
import { removeAsyncStorage } from '../Helper/Storage';
import { LoginManager } from 'react-native-fbsdk';

function logout(p) {
  removeAsyncStorage('token');
  LoginManager.logOut();
  const { navigate } = p.navigation;
  navigate("NoLogin");
}

class CustomDrawer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      company_name: '',
      avatar: 'https://globly.eu/wp-content/uploads/2019/11/1-13-150x150.jpg',
      email: '',
    }
    // getData('@nickname').then(result => this.setState({ company_name: result }));
  }

  render() {
    return (
      <ScrollView style={{ flex: 1, backgroundColor: '#fff267'}}>
        <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }} style={{ marginTop: 150 }}>
          <View style={{ flexDirection: 'row' }}>
            <View>
              {/* <Image
                  source={{ uri:this.state.avatar}}
                  style={{width: 60, height: 60, borderRadius: 60/ 2, marginLeft:40}}/> */}
            </View>
            <View style={{ flex: 1 }}>
              {/* <Text style={{ alignSelf: 'center', paddingTop: 20, color: 'white', fontSize: 18 }}>
                {((this.state.company_name).length > 10) ? (((this.state.company_name).substring(0, 9)) + '...') :
                  this.state.company_name}</Text> */}
            </View>
          </View>
          <View style={{ marginLeft: 40, marginTop: 20 }}>
            <DrawerItems {...this.props} labelStyle={{ color: '#727272' }} />
          </View>
          <TouchableOpacity onPress={() => logout(this.props)}>
            <View style={styles.item}>
              <View style={styles.iconContainer}>
                <Image source={require('../../images/logout-icon.png')} style={styles.icon}></Image>
              </View>
              <Text style={styles.label}>Wyloguj się</Text>
            </View>
          </TouchableOpacity>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 40
  },
  label: {
    margin: 16,
    fontWeight: 'bold',
    color: '#727272',
  },
  iconContainer: {
    marginHorizontal: 16,
    width: 24,
    alignItems: 'center',
  },
  icon: {
    width: 20,
    height: 20,
    tintColor: 'white'
  }
});
export { CustomDrawer };
