import { StyleSheet,Dimensions } from 'react-native';
var width = Dimensions.get('window').width; 
'use strict';
const styles = StyleSheet.create({
    mainText: {
        fontSize: 25,
        padding: 10,
        fontWeight: '600',
        marginTop: 20,
        marginLeft: 10
    },
    inputs: {
        padding: 20
    },
    buttonmargin: {
        marginTop: 10
    },

    textInput: {
        color: '#0b032d',
        height: 50,
        borderRadius: 25,
        borderWidth: 0.6,
        marginHorizontal: 20,
        paddingLeft: 10,
        marginVertical: 5,
        borderColor: 'rgba(0,0,0,0.3)',
        zIndex:1
    },

    button: {
        backgroundColor: 'white',
        height: 50,
        marginHorizontal: 20,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 5
    }

});
export { styles };
