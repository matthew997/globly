import { StyleSheet, Dimensions } from 'react-native';
import * as theme from '../Style/Theme';

const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    position: 'relative',
    width,
    height: width,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'yellow'
  },
  price: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#f6921e',
  },
  circle: {
    position: 'absolute',
    top: -theme.sizes.margin,
    left: theme.sizes.margin,
    width: theme.sizes.padding * 2,
    height: theme.sizes.padding * 2,
    borderRadius: theme.sizes.padding,
    backgroundColor: "#E8F4F9"
  },
  status: {
    backgroundColor: '#f6921e',
    height: 30,
    fontFamily:"Nunito-Bold",
    marginHorizontal: 20,
    borderRadius: 35,
    padding: 5,
    
  },
  button: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: '#f6921e',
    height: 45,
    width: 250,
    borderRadius: 35,
    // paddingTop: 0,
    // paddingLeft:5,
    alignSelf:'center'

  },
  map: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  carousel: {
    position: 'absolute',
    bottom: 0,
  },
  flex: {
    flex: 0,
  },
  column: {
    flexDirection: 'column'
  },
  row: {
    flexDirection: 'row'
  },
  header: {
    paddingHorizontal: theme.sizes.padding,
    paddingTop: theme.sizes.padding,
    justifyContent: 'space-between',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  back: {
    width: theme.sizes.base * 3,
    height: theme.sizes.base * 3,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  contentHeader: {
    backgroundColor: 'transparent',
    padding: theme.sizes.padding,
    backgroundColor: theme.colors.white,
    borderTopLeftRadius: theme.sizes.radius,
    borderTopRightRadius: theme.sizes.radius,
    marginTop: -theme.sizes.padding / 2,
  },
  shadow: {
    shadowColor: theme.colors.black,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
  like:{
    marginLeft: 5,
    marginTop: 5,
    width: 60,
    height: 60,
    borderRadius: 30,
    paddingTop: 15,
    paddingLeft: 15
},
  TouchableOpacityStyle:{
  
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    bottom: 30,
    left: theme.sizes.margin,
    width: theme.sizes.padding * 2,
    height: theme.sizes.padding * 2,
    borderRadius: theme.sizes.padding,
    backgroundColor: "#049804"
    
  },
  dotsContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 36,
    right: 0,
    left: 0
  },
  dots: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 6,
    backgroundColor: theme.colors.gray,
  },
  title: {
    fontFamily:"Nunito-Bold",
    fontSize: theme.sizes.font * 2,
    
  },
  description: {
    fontSize: theme.sizes.font * 1.2,
    lineHeight: theme.sizes.font * 2,
    color: theme.colors.caption,
  },
  div: {
    // fontSize: theme.sizes.font * 1.2,
    lineHeight: theme.sizes.font * 2,
    color: theme.colors.caption,
    fontFamily: "Nunito-Light"
  },
  TextBold: {
    fontSize: theme.sizes.font * 1.2,
    lineHeight: theme.sizes.font * 2,
    color: theme.colors.caption,
    fontFamily: "Nunito-Bold"
  },
  Text: {
    fontSize: theme.sizes.font * 1.0,
    lineHeight: theme.sizes.font * 2,
    color: theme.colors.caption,
    fontFamily: "Nunito-Light",
    fontWeight:"900"

  },
});

export { styles };