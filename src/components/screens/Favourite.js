import React, { Component } from 'react';
import {
    Text,
    View,
    FlatList,
    StyleSheet,
    ActivityIndicator,
    Dimensions,
    Image,
    TouchableOpacity
} from 'react-native';
// import AsyncStorage from '@react-native-community/async-storage';
import { getData } from '../Helper/Storage';
var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: 'UserDatabase.db' });
var width = Dimensions.get('window').width;

export default class Favourite extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            id: "",
            page: 1,
            error: "",
            like: false,
            nickname: ''
        };
        getData('@nickname').then(result => this.setState({ nickname: result }));
        this.LikeStorage();

    }
    componentDidMount() {
        this.subs = this.props.navigation.addListener("didFocus", () => {
            console.log("update")
            this.LikeStorage();
        });
    }

    LikeStorage() {
        var id = this.state.id;
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM Favourites', [], (tx, results) => {
                var temp = [];
                for (let i = 0; i < results.rows.length; ++i) {
                    temp.push(results.rows.item(i)['postId']);
                }
                let s = JSON.stringify(temp);
                let d = s.replace(/,/g, '-');
                let dd = d.slice(1, -1);

                this.setState({ id: dd });

                this.featchData();

            });
        });
    }
    async featchData() {

        const response = await fetch(
            'https://globly.eu/wp-json/wp/v2/apartments/arr?array=' + this.state.id+"&page="+this.state.page,
        );
        const json = await response.json();

        this.setState(state => ({
            data: [...json],
            loading: false
        }));
    };

    handleEnd = () => {
        // this.setState({ page: this.state.page + 1 }, () => this.featchData());
    }
    render() {

        return (
            <View style={styles.container}>
                <Text style={{ textAlign: 'center', fontSize: 17, marginTop: 7, marginBottom: 7, fontFamily: "Nunito-Bold" }}>{this.state.nickname}, oto Twoje ulubione oferty.</Text>
                <FlatList
                    data={this.state.data}
                    keyExtractor={item => item.slug}
                    onEndReached={() => this.handleEnd()}
                    initialNumToRender={10}
                    onEndReachedThreshold={0.5}
                    ListFooterComponent={() =>
                        this.state.loading
                            ? null
                            : <Text style={{textAlign:'center', fontFamily: "Nunito-Light"}}>To już wszystkie oferty!</Text>}
                    renderItem={({ item }) => <Item post={item} nav={this.props.navigation} />}
                />

            </View>
        );
    }
}
class Item extends Component {
    numberWithCommas(x) {
        if (x === 0) {
            return this.props.post.price_before;
        } else {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' €';
        }
    }
    titleShorter(x) {
        if ((x).length > 50) {
            return (((x).substring(0, 47)) + '...');
        } else {
            return x;
        }
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white', marginBottom: 5, paddingLeft: 7, paddingBottom: 10 }}>
                <View style={{ position: 'absolute', width: 30, height: 30, marginLeft: 85, marginTop: 12, zIndex: 999 }}>
                    <Image
                        source={require('../../images/like-2.png')}
                        style={{ width: 20, height: 20 }} />

                </View>
                <View style={styles.image}>
                    <Image
                        source={{ uri: this.props.post.featured_image.thumbnail }}
                        style={{ width: 100, height: 100, borderBottomLeftRadius: 7, borderTopLeftRadius: 7 }} />
                </View>
                <View style={{ position: 'absolute', marginLeft: 110 }}>
                    <Text style={styles.item}>{this.titleShorter(this.props.post.title)}</Text>
                    <Text style={styles.text2}>{this.numberWithCommas(this.props.post.price)}</Text>
                    <TouchableOpacity style={{
                        ...styles.button,
                        backgroundColor: '#f6921e',
                    }}
                        onPress={() => this.props.nav.navigate('Single', { id: this.props.post.id })
                        }>
                        <Text style={{ fontSize: 17, fontFamily: "Nunito-Bold", color: 'white' }}>
                            Odkryj
                    </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}


export { Favourite };
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 0
    },
    item: {
        // textAlign: 'justify',
        fontWeight: 'bold',
        padding: 9,
        paddingRight: 10,
        fontSize: 16,
        width: width - 110,


    },
    text2: {
        marginTop: 10,
        marginLeft: 15,
        fontSize: 20,
        fontFamily: "Nunito-Bold",
        color: 'black',
        backgroundColor: '#fff278',
        alignSelf: 'flex-start',
        padding: 5
    },
    button: {
        // position:'relative',
        marginLeft: width - 225,
        marginTop: -33,
        backgroundColor: 'white',
        height: 30,
        width: 100,
        marginHorizontal: 20,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 5
    },
    image: {
        width: 100,
        height: 100,
        position: 'relative',
        marginTop: 10,
        flexDirection: 'row',
    }
})

