import React, { Component } from 'react';
import {
    View,
    Dimensions, Image, Text, TouchableOpacity, StyleSheet,StatusBar
} from 'react-native';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import {FBLogin} from '../screens/Login/FbLogin';


class NoLogin extends Component {

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <View style={{ flex: 1, width: width, padding: 20, justifyContent: 'center' }}>
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                        marginTop: height / 2 - 100,
                    }}>
                        <Image source={require('../../images/logo.png')} style={{ width: 250, height: 250 }} />
                    </View>
                    <View style={{ marginTop: height / 2 - 190 }}>
                        <TouchableOpacity style={{
                            ...styles.button,
                            backgroundColor: '#EFF3F3',
                        }}
                            onPress={() => {
                                this.props.navigation.navigate('Login', {
                                    otherParam: this.props.navigation,
                                  });
                            
                            }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'grey' }}>
                                Zaloguj się
                            </Text>
                        </TouchableOpacity>
                        <FBLogin {...this.props}/>
                        <TouchableOpacity style={{
                            ...styles.button,
                            backgroundColor: '#de5246',
                        }}
                            onPress={() => {
                                alert("Logowanie przez Google nie jest jeszcze dostępne, pracujemy nad tym...")
                            }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
                                Zaloguj się przez Google
                        </Text>
                        </TouchableOpacity>

                       
                       
                    </View>
                </View>
            </View>
        );
    }
};

export { NoLogin };

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        backgroundColor: 'white',
        height: 50,
        marginHorizontal: 20,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 5
    }
});