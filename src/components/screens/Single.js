import React, { Component } from 'react'
import {
    Text,
    View,
    Animated,
    Image,
    Dimensions,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    Linking, BackHandler
} from 'react-native'
import likeImg from '../../images/like-2.png';
import dontLike from '../../images/like.png';
import HTMLView from 'react-native-htmlview';
import MapView, { Marker } from 'react-native-maps';
import * as theme from '../Style/Theme';
import { styles } from '../Style/SinglePage';
import { Details } from './Single/Details';


var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: 'UserDatabase.db' });

const { width, height } = Dimensions.get('window');


class Single extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            likeData: [],
            id: this.props.navigation.getParam('id'),
            loading: true,
            like: false,
            likeImg: "",
        };
        this.featchData();
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {

        return false;  // Do nothing when back button is pressed
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.subs = this.props.navigation.addListener("didFocus", () => {
            this.LikeStorage()
        });
    }

    LikeStorage() {
        var id = this.state.id;
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM Favourites', [], (tx, results) => {
                var temp = [];
                for (let i = 0; i < results.rows.length; ++i) {
                    temp.push(results.rows.item(i)['postId']);
                }
                this.setState({
                    data: temp,
                });
                let data2 = temp.filter(function (e) {
                    return e == id;
                });
                if (data2[0]) {
                    this.setState({ like: true });
                }
            });
        });
    }

    async featchData() {

        const response = await fetch(
            'https://globly.eu/wp-json/wp/v2/apartment/' + this.state.id,
        );
        const json = await response.json();
        this.setState(state => ({
            data: json,
            loading: false,
        }));
    };
    scrollX = new Animated.Value(0);
    numberWithCommas(x) {
        if (x === 0) {
            return this.state.data.price_before;
        } else {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' €';
        }
    }
    repleace = (e) => {
        e = e.replace("<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\" target=\"_top\">\r\n<input type=\"hidden\" name=\"cmd\" value=\"_s-xclick\">\r\n<input type=\"hidden\" name=\"hosted_button_id\" value=\"YED5LQGHMSLB6\">\r\n<input type=\"image\" src=\"https://www.paypalobjects.com/pl_PL/PL/i/btn/btn_paynowCC_LG.gif\" border=\"0\" name=\"submit\" alt=\"PayPal – Płać wygodnie i bezpiecznie\">\r\n<img alt=\"\" border=\"0\" src=\"https://www.paypalobjects.com/pl_PL/i/scr/pixel.gif\" width=\"1\" height=\"1\">\r\n</form>", '').replace('[contact-form-7 id=\"11697\" title=\"#FORM 1A\"]', '');
        return '<div>' + e.substring(0, 150) + '...</div>';
    }
    renderDots = () => {
        const dotPosition = Animated.divide(this.scrollX, width);
        if (this.state.loading) {
            return <Text>Loading</Text>;
        }
        return (
            <View style={[styles.flex, styles.row, styles.dotsContainer]}>

                {this.state.data.images.map((item, index) => {
                    const opacity = dotPosition.interpolate({
                        inputRange: [index - 1, index, index + 1],
                        outputRange: [0.5, 1, 0.5],
                        extrapolate: 'clamp'
                    });
                    return (
                        <Animated.View
                            key={`step-${item.key}`}
                            style={[styles.dots, { opacity }]}
                        />
                    )
                })}
            </View>
        )
    }
    like() {
        let a = [];
        let id = this.state.id;
        let status;

        if (this.state.like == false) {
            db.transaction(function (tx) {
                tx.executeSql(
                    'INSERT INTO Favourites (postId) VALUES (?)',
                    [id],
                );
            });
            status = true;
        } else {
            db.transaction(function (tx) {
                tx.executeSql(
                    'DELETE FROM  Favourites where postId=?',
                    [id],
                );
            });
            status = false;
        }

        this.setState({ like: status })

    }

    render() {
        var like = this.state.like ? likeImg : dontLike;
        if (this.state.loading) {
            return <ActivityIndicator size="large" animating style={{ alignItems: 'center', marginTop: height / 4 - 20 }} color="#fff267" />;
        }
        return (
            <View>
                <ScrollView style={styles.flex}>
                    <View style={[styles.flex]}>
                        <View style={{ backgroundColor: '#eaf3ff', position: 'absolute', width, height: width }}>
                            <ActivityIndicator size="large" animating style={{ marginTop: height / 4 - 20 }} color="#fff267" />
                        </View>
                        <ScrollView
                            horizontal
                            pagingEnabled
                            scrollEnabled
                            showsHorizontalScrollIndicator={false}
                            decelerationRate={0}
                            scrollEventThrottle={16}
                            snapToAlignment="center"
                            onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: this.scrollX } } }])}
                        >
                            {
                                this.state.data.images.map((item) =>
                                    <Image
                                        key={`image-${item.key}`}
                                        source={{ uri: item.image }}
                                        resizeMode='cover'
                                        style={{ width, height: width }}
                                    />
                                )
                            }

                        </ScrollView>
                        {this.renderDots()}
                    </View>
                    <View style={[styles.flex, styles.content]}>
                        <View style={[styles.flex, styles.contentHeader]}>
                            <View style={styles.circle}>
                                <TouchableOpacity style={styles.like}
                                    onPress={() => this.like()}>
                                    <Image
                                        source={like}
                                    />

                                </TouchableOpacity>
                            </View>
                            <Text style={styles.title}>{this.state.data.title}</Text>
                            <View style={[
                                styles.row,
                                { alignItems: 'center', marginVertical: theme.sizes.margin / 2 }
                            ]}>
                                <Text style={styles.price}>
                                    {this.numberWithCommas(this.state.data.price)}
                                </Text>
                                {this.state.data.status == "" ? <View></View> : <View style={styles.status}><Text style={{ textAlign: 'center', color: 'white' }}>{this.state.data.status}</Text></View>}
                            </View>
                            <View>
                                <HTMLView
                                    value={this.repleace(this.state.data.content)}
                                    stylesheet={styles.description}
                                />
                            </View>
                            <TouchableOpacity style={styles.button}
                                onPress={() => { Linking.openURL("https://globly.eu/nieruchomosci/" + this.state.data.slug + "?utm_source=app101&utm_medium=app101_button&utm_campaign=globlito_app101") }}>
                                <Text style={{ color: 'white', fontFamily: "Nunito-Bold" }}>Interesuje mnie ta oferta</Text>
                            </TouchableOpacity>

                            <Details data={this.state.data} />

                        </View>
                    </View>
                    <View style={styles.container}>
                        <MapView style={styles.map}
                            initialRegion={{
                                latitude: this.state.data.geo[0].latitude,
                                longitude: this.state.data.geo[0].longitude,
                                latitudeDelta: 0.015 * 5,
                                longitudeDelta: 0.0121 * 5,
                            }}
                        >

                            <Marker
                                key={'32434'}
                                coordinate={this.state.data.geo[0], this.state.data.geo[0]}
                            />

                        </MapView>

                    </View>
                </ScrollView>
                <TouchableOpacity activeOpacity={0.5} style={styles.TouchableOpacityStyle} onPress={() => { Linking.openURL(`tel:+48 509 694 441`)}}>
                    <Image source={require('../../images/phone.png')} style={{ width:40, height:40}}
                         />
                </TouchableOpacity>
            </View>
        )
    }
}

export { Single };

