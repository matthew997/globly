import React, { Component } from 'react';
import { TextInput, View, Text } from 'react-native';

var types = ['email', 'password'];
class InputText extends Component {
    constructor(props) {
        super(props);
        this.state = { "text": "", "touched": false, 'error': false };
    }
    alph = {
        // 'email':/^([\w\d\-\.]+)@{1}(([\w\d\-]{1,67})|([\w\d\-]+\.[\w\d\-]{1,67}))\.(([a-zA-Z\d]{2,4})(\.[a-zA-Z\d]{2})?)$/,
        'email': /^.{4,}$/,
        'password': /^.{4,}$/,

    }
    errorMessages = {
        'email': 'wprowadź Email lub Login',
        'password': "Hasło musi składać się z dużych i małych liter oraz cyfr"
    };
    sendData(e, type) {
        this.setState({ touched: true });
        if (this.alph[type].test(e)) {
            this.setState({ error: true });
            this.props.onChangeText(e, true);
        } else {
            this.setState({ error: false });
            this.setState({ message: this.errorMessages[type] });
            this.props.onChangeText(e, false);
        }

    }
    render() {

        return (
            <View style={{flex:1}}>
                <View style={{ marginTop: 20, marginBottom: -15, flex:1}}>
                    {/* <Text style={{ backgroundColor: 'white', alignSelf: 'flex-start', marginLeft: 40, position: 'relative', zIndex: 99999 }}>{this.props.label}</Text> */}
                </View>
                <TextInput
                    {...this.props}
                    autoFocus={this.props.focus}
                    placeholder={this.props.label}
                    placeholderTextColor = "#f6921e"
                    secureTextEntry={this.props.secure}
                    onChangeText={(value) => {
                        this.setState({ "text": value });
                        this.sendData(value, this.props.type)
                    }}
                    autoCapitalize='none'
                    style={[this.props.style, { borderColor: (!this.state.error && this.state.touched) ? 'red' : 'lightgray' }]}
                />
                {!this.state.error && this.state.touched && <Text style={{ fontSize: 12, color: 'red', textAlign: 'center' }}>{this.state.message}</Text>}
            </View>
        );
    }
}


export { InputText };

