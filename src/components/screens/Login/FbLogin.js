import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import { storeData } from '../../Helper/Storage';
class FBLogin extends Component {
    constructor(props) {
        super(props);
        this.FbToken;
        this.user_login;
        this.cookie;
        console.log(this.props);
        this.state = {
            loading: false
        }
    }

    handleFacebookLogin() {
        this.setState({ loading: true });
        LoginManager.logInWithPermissions(["public_profile"]).then(
            (result) => {
              
                if (result.isCancelled) {
                   LoginManager.logOut();
                    this.setState({ loading: false });
                    console.log("Login cancelled");
                 
                } else {
                    
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                           console.log(data);
                            this.FbToken = data.accessToken;
                            this.initUser();
                        });
                }
            }
        );
    }
    initUser() {
        fetch('https://globly.eu/api/user/fb_connect?access_token=' + this.FbToken)
            .then((response) => response.json())
            .then((json) => {
                this.login(json);
            })
            .catch(() => {
                reject('ERROR GETTING DATA FROM FACEBOOK')
                alert("Error Getting Data From Facebook");
            })
    }
    login(json) {
        if (json.msg == "user logged in." || json.msg == "user registered.") {
            this.user_login = json.user_login;
            this.cookie = json.cookie;
            storeData("token", json.cookie);
            this.setState({ loading: false });
            this.props.navigation.navigate('Home');
        } else {
            this.setState({ loading: false });
            alert("Coś poszło nie tak...");
        }
    }
    fblogout() {
        LoginManager.logOut();
    }



    render() {

        return (
            <View>
                {this.state.loading == true ?
                    <View style={[styles.container, styles.horizontal]}>
                        <ActivityIndicator size="large" color="#fff267" />
                    </View> : <View></View>}
                <TouchableOpacity style={{
                    ...styles.button,
                    backgroundColor: '#3b5998',
                }}
                    onPress={() => {


                        this.handleFacebookLogin()
                    }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
                        Zaloguj się przez Facebook
                        </Text>
                </TouchableOpacity>

            </View>
        );
    }
};


export { FBLogin };

const styles = StyleSheet.create({
    button: {
        backgroundColor: 'white',
        height: 50,
        marginHorizontal: 20,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 5
    },
    container: {
        backgroundColor: 'white',
        position: "absolute",
        flex: 2,
        justifyContent: 'center',
        zIndex:999,
        alignSelf:'center',
        marginTop:-250

    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
});
