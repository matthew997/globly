import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    TextInput,
    Button,
    TouchableOpacity
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { LogoTitle } from '../../components/Helper/Header';
import { styles } from '../../components/Style/LoginStyle';
var width = Dimensions.get('window').width;

class Register extends Component {
    static navigationOptions = {
        headerTitle: <LogoTitle />,
        headerTintColor: 'grey',
        headerStyle: {
            backgroundColor: '#FFEC51',
        },
    };
    errorMessages = {
        '0': "To pole jest wymagane",
        '1': 'Nieprawidłowy adres email',
        '2': "Nazwa użytkownika musi składać sie z co najmniej 6 znaków",
        '3': "Numer telefonu jest nieprawidłowy",
        '4': "Hasła nie mogą się różnić",
        '5': "Hasło powinno składać się z 6 znaków i 2 cyfr"
    };
    state = {
        username: '',
        email: '',
        password: '',
        passwordRepeat: '',

        usernameValid: true,
        emailValid: true,
        passwordValid: true,
        passwordRepeatValid: true,

        usernameTouched: false,
        emailTouched: false,
        passwordTouched: false,
        passwordRepeatTouched: false,
        message: "",
    };
    alph = {
        'email': /^([\w\d\-\.]+)@{1}(([\w\d\-]{1,67})|([\w\d\-]+\.[\w\d\-]{1,67}))\.(([a-zA-Z\d]{2,4})(\.[a-zA-Z\d]{2})?)$/,
        'username': /^.{6,}$/,
        'password': /^(?=.*[a-z])(?=.*[0-9]).{6,20}$/,

    }
    checkUsername(e) {
        this.setState({ usernameTouched: true });
        if (this.alph['username'].test(e)) {
            this.setState({ usernameValid: true });
        } else {
            this.setState({ usernameValid: false });
        }
    }
    checkEmail(e) {
        this.setState({ emailTouched: true });
        if (this.alph['email'].test(e)) {
            this.setState({ emailValid: true });
        } else {
            this.setState({ emailValid: false });
        }
    }
    checkPassword(e) {
        this.setState({ passwordTouched: true });
        if (this.alph['password'].test(e)) {
            this.setState({ passwordValid: true });
        } else {
            this.setState({ passwordValid: false });
        }
    }
    checkPasswordRepeat(e) {
        this.setState({ passwordRepeatTouched: true });
        if (e == this.state.password) {
            this.setState({ passwordRepeatValid: true });
        } else {
            this.setState({ passwordRepeatValid: false });
        }
    }
    RegisterToApp() {
        if (this.state.usernameValid &&
            this.state.emailValid &&
            this.state.passwordValid &&
            this.state.passwordRepeatValid) {

            fetch('https://globly.eu/api/user/register/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: 'username=' + this.state.username + '&user_pass=' + this.state.password +'&email='+ this.state.email +'&display_name='+ this.state.username+'&nonce=null'

            }).then((response) => {
                response.json().then((jsonData) => {
                    if(jsonData.status == "ok"){
                        this.props.navigation.navigate('Login', {
                            itemId: 86,
                            otherParam: 'First Details',
                        });
                        alert("GRATULACJE, TWOJE KONTO ZOSTAŁO ZAŁOŻONE");
                    }else{
                        this.setState({message:jsonData.error});
                    }
                });
            });
        

    }else{
    this.setState({ message: "Sprawdź czy wszystkie pola są uzupełnione poprawnie!" });
}
    }

render() {
    let { username, email, passwordRepeat, password } = this.state;
    return (
        <View style={{ flex: 1 }}>
            <Text style={styles.mainText}>Rejestracja</Text>
            <View style={{ flex: 1, padding: 20 }}>
                <ScrollView>

                    <View style={{ paddingBottom: 20 }}>
                        <TextInput
                            label="Nazwa użytkownika"
                            placeholder="Nazwa użytkownika"
                            placeholderTextColor="#f6921e"
                            value={username}
                            onChangeText={(username) => {
                                this.setState({ username });
                                this.checkUsername(username)
                            }}
                            style={[styles.textInput, { borderColor: (!this.state.usernameValid && this.state.usernameTouched) ? 'red' : 'grey' }]}

                        />
                        {this.state.usernameValid == false ? (
                            <Text style={{ fontSize: 12, color: 'red', textAlign: 'center' }}>
                                {this.errorMessages[2]}
                            </Text>
                        ) : null}

                        <TextInput
                            label="Email"
                            placeholder="Email"
                            placeholderTextColor="#f6921e"
                            value={email}
                            onChangeText={(email) => {
                                this.setState({ email });
                                this.checkEmail(email)
                            }}
                            style={[styles.textInput, { borderColor: (!this.state.emailValid && this.state.emailTouched) ? 'red' : 'grey' }]}

                        />
                        {this.state.emailValid == false ? (
                            <Text style={{ fontSize: 12, color: 'red', textAlign: 'center' }}>
                                {this.errorMessages[1]}
                            </Text>
                        ) : null}
                        <TextInput
                            label="Hasło"
                            placeholder="Hasło"
                            placeholderTextColor="#f6921e"
                            value={password}
                            secureTextEntry={true}
                            onChangeText={(password) => {
                                this.setState({ password });
                                this.checkPassword(password)
                            }}
                            style={[styles.textInput, { borderColor: (!this.state.passwordValid && this.state.passwordTouched) ? 'red' : 'grey' }]}
                        />
                        {this.state.passwordValid == false ? (
                            <Text style={{ fontSize: 12, color: 'red', textAlign: 'center' }}>
                                {this.errorMessages[5]}
                            </Text>
                        ) : null}
                        <TextInput
                            label="Powtórz Hasło"
                            placeholder="Powtórz Hasło"
                            placeholderTextColor="#f6921e"
                            value={passwordRepeat}
                            secureTextEntry={true}
                            onChangeText={(passwordRepeat) => {
                                this.setState({ passwordRepeat });
                                this.checkPasswordRepeat(passwordRepeat)
                            }}
                            style={[styles.textInput, { borderColor: (!this.state.passwordRepeatValid && this.state.passwordRepeatTouched) ? 'red' : 'grey' }]}
                        />
                        {this.state.passwordRepeatValid == false ? (
                            <Text style={{ fontSize: 12, color: 'red', textAlign: 'center' }}>
                                {this.errorMessages[4]}
                            </Text>
                        ) : null}
                        <Text style={{ color: 'red', textAlign: 'center' }}>{this.state.message != "" ? this.state.message : ""}</Text>

                    </View>
                    <View>
                        <TouchableOpacity style={{
                            ...styles.button,
                            backgroundColor: '#f6921e',
                        }}
                            onPress={() => this.RegisterToApp()}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
                                Zarejestruj się
                        </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.buttonmargin}>
                        <Text style={{ textAlign: 'center', color: 'gray', marginTop: 10 }}>
                            MASZ JUŻ KONTO?
                            </Text>
                        <TouchableOpacity

                            onPress={() =>
                                this.props.navigation.navigate('Login', {
                                    itemId: 86,
                                    otherParam: 'First Details',
                                })
                            }>
                            <Text style={{ fontSize: 15, color: '#f6921e', fontWeight: 'bold', textAlign: 'center' }}>
                                Zaloguj się
                        </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        </View>
    )
}
};


const styless = StyleSheet.create({
    mainText: {
        fontSize: 25,
        padding: 10,
        fontWeight: '600',
        marginTop: 20,
        marginLeft: 10
    },
    buttonmargin: {
        marginTop: 20
    },
    inputStyle: {
        marginTop: 20,
        borderColor: 'lightgray',
        borderWidth: 1,
        padding: 5,
        height: 45,
        fontSize: 19,
        backgroundColor: 'white',
        color: 'red'
    },
    error: {

    }
});
export { Register }
