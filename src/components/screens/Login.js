import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView, 
  TouchableOpacity, 
  StyleSheet,
  ActivityIndicator
} from 'react-native';

import {storeData} from '../Helper/Storage';
import { LogoTitle } from '../Helper/Header';
import { InputText } from './Login/LoginInputText';
import { styles } from '../Style/LoginStyle';





class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "", pass: "", emailError: null, passError: null, loading: false,
      isLogin: false,
      error: false, message: ""
    };
    this.nav = this.props.navigation.getParam('otherParam');
  }
  static navigationOptions = {
    headerTitle: <LogoTitle />,
    headerTintColor: 'grey',
    headerStyle: {
      backgroundColor: '#FFEC51',
    },
  };

  loginToApp() {

    this.setState({ "loading": true });
    if (this.state.emailError && this.state.passError) {
      this.setState({ loading: true });
      this.oAuthLogin();
    } else {
      this.setState({ loading: false, error: true, message: "Uzupełnij wszystkie pola!" })
    }
    setTimeout(() => {
      this.setState({ "loading": false });
    }, 2500);

  }

  oAuthLogin() {
    fetch('https://globly.eu/api/user/generate_auth_cookie/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: 'username=' + this.state.email + "&password=" + this.state.pass

    }).then((response) => {
      response.json().then((jsonData) => {
        this.endLogin(jsonData);
      });
    });
  }
  endLogin = (jsonData) => {
    if (jsonData.status == "ok") {
      storeData("token",jsonData.cookie);
      this.nav.navigate("Home");
    } else {
      this.setState({ loading: false, error: true, message: "Wprowadzony Login lub Hasło jest niepoprawny." })
    }

  }

  render() {
    if (this.state.loading) {
      return (
        <View style={[styleess.container, styleess.horizontal]}>
          <ActivityIndicator size="large" color="#f7941e" />
        </View>
      )
    }
    return (
      <View style={{ flex: 1 }}>

        <View style={{ flex: 1 }}>
          <Text style={styles.mainText}>Zaloguj się</Text>
          <ScrollView style={{ flex: 1 }}>
            <View style={styles.inputs}>

              <InputText label='Email lub Login' type='email' focus={false} style={styles.textInput}
                onChangeText={(email, error) => { this.setState({ "email": email, "emailError": error }); }} />

              <InputText label='Hasło' type='password' secure={true} focus={false} style={styles.textInput}
                onChangeText={(pass, error) => { this.setState({ "pass": pass, "passError": error }); }} />

              
                <Text style={{ color: 'red', textAlign: 'center' }}>{this.state.error == true ? this.state.message : ""}</Text>
                <View style={styles.buttonmargin}>
                <TouchableOpacity style={{
                  ...styles.button,
                  backgroundColor: '#f6921e',
                }}
                  onPress={() => this.loginToApp()}>
                  <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>
                    Zaloguj się
                        </Text>
                </TouchableOpacity>

                <Text style={{ textAlign: 'center', color: 'gray', marginTop: 10 }}>
                  NIE MASZ KONTA?
                </Text>
                <TouchableOpacity

                  onPress={() =>
                    this.props.navigation.navigate('Register', {
                      itemId: 86,
                      otherParam: 'First Details',
                    })
                  }>
                  <Text style={{ fontSize: 15, color: '#f6921e', fontWeight: 'bold', textAlign: 'center' }}>
                    Zarejestruj się
                        </Text>
                </TouchableOpacity>


              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    )
  }
};


export { Login };

const styleess = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
})