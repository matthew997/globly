import React, { Component } from 'react';


export default class SingleMain extends Component {
    render() {
        return (
            <View style={[styles.flex, styles.content]}>
            <View style={[styles.flex, styles.contentHeader]}>
                <View style={styles.circle}>
                <TouchableOpacity style={{ marginLeft: 20, marginTop:20 }}>
                    {this.state.icon}
                        <Image
                            source={require('../../images/like.png')}
                         />
                
                </TouchableOpacity>
                </View>
                <Text style={styles.title}>{this.state.data.title}</Text>
                <View style={[
                    styles.row,
                    { alignItems: 'center', marginVertical: theme.sizes.margin / 2 }
                ]}>
                    <Text style={styles.price}>
                        {this.state.data.price}{this.state.data.wpestate_currency}
                    </Text>
                </View>
                <HTMLView
                    value={this.state.data.property.text}
                    stylesheet={styles}
                />
                <HTMLView
                    value={this.state.data.property.features}
                    stylesheet={styles}
                />
                <TouchableOpacity>
                    <HTMLView
                        value={this.repleace(this.state.data.content)}
                        stylesheet={styles.description}
                    />
                </TouchableOpacity>
            </View>
        </View>
        );
    }
}



export { SingleMain };




