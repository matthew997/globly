import React, { Component } from 'react'
import {
    Text,
    View,
    Image
} from 'react-native';
import HTMLView from 'react-native-htmlview';
import { styles } from '../../Style/SinglePage';

class Details extends Component {
    remove(text, e) {
        return text.replace(e, '');
    }
    data = this.props.data;
    Address = (a, name) => {
        if (this.data.address.Address) {
            return (
                <View style={{ flexDirection: 'row', width: '100%' }}>
                    <Text style={styles.TextBold}>{name}</Text><Text style={styles.Text}> {a}</Text>
                </View>
            )
        }
    }

    LotSize = () => {
        if (this.data.property.property_lot_size) {
            return (
                <View style={{ flexDirection: 'row', width: '100%' }}>
                    <Text style={styles.TextBold}>Powierzchnia działki:</Text><Text style={styles.Text}> {this.remove(this.data.property.property_lot_size, '<sup>2</sup>')}2</Text>
                </View>
            )
        }
    }
    FlatSize = () => {
        if (this.data.property.FlatSize) {
            return (
                <View style={{ flexDirection: 'row', width: '100%' }}>
                    <Text style={styles.TextBold}>Powierzchnia:</Text><Text style={styles.Text}> {this.remove(this.data.property.FlatSize, '<sup>2</sup>')}2</Text>
                </View>
            )
        }
    }
    
    property_rooms = () => {
        if (this.data.property.property_rooms) {
            return (
                <View style={{ flexDirection: 'row', width: '50%' }}>
                    <Text style={styles.TextBold}>Pokoje:</Text><Text style={styles.Text}> {this.data.property.property_rooms}</Text>
                </View>
            )
        }
    }
    property_bathrooms = () => {
        if (this.data.property.property_bathrooms) {
            return (
                <View style={{ flexDirection: 'row', width: '50%' }}>
                    <Text style={styles.TextBold}>Łazienki:</Text><Text style={styles.Text}> {this.data.property.property_bathrooms}</Text>
                </View>
            )
        }
    }
    property_bedrooms = () => {
        if (this.data.property.property_bedrooms) {
            return (
                <View style={{ flexDirection: 'row', width: '50%' }}>
                    <Text style={styles.TextBold}>Sypialnie:</Text><Text style={styles.Text}> {this.data.property.property_bedrooms}</Text>
                </View>
            )
        }
    }
    features = () => {
        if (this.data.property.features) {
            return (
                <View>
                    <Text style={{ fontSize: 25, fontFamily:"Nunito-Bold", marginTop: 10 }}>Informacje</Text>
                    <View style={{ width: 40, height: 7, backgroundColor: '#FF674D', marginTop: 5, marginBottom: 5 }} />

                    <View style={{ flexDirection: 'row', width: '50%' }}>
                        <HTMLView
                            value={this.data.property.features}
                        />
                    </View>
                </View>
            )
        }
    }
    property_more = (a, name) => {
        if (a) {
            return (
                <View style={{ flexDirection: 'row', width: '50%' }}>
                    <Image
                        source={require('../../../images/tick.png')}
                        style={{ width: 15, height: 15, marginTop: 6 }}
                    />
                    <Text style={styles.Text}>{name}</Text>
                </View>
            )
        }
    }
    render() {
        return (
            <View>
                <Text style={{ fontSize: 25, fontFamily:"Nunito-Bold", }}>Adres</Text>
                <View style={{ width: 40, height: 5, backgroundColor: '#f6921e', marginTop: 5, marginBottom: 5 }} />
                {this.Address(this.data.address.Address, "Adres:")}
                {this.Address(this.data.address.property_city, "Lokalizacja:")}
                {this.Address(this.data.address.propertyCountry, "Kraj:")}


                <Text style={{ fontSize: 25, fontFamily:"Nunito-Bold", }}>Szczegóły</Text>
                <View style={{ width: 40, height: 5, backgroundColor: '#f6921e', marginTop: 5, marginBottom: 5 }} />
                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    alignItems: 'flex-start' // if you want to fill rows left to right
                }}>
                    <View style={{ flexDirection: 'row', width: '100%' }}>
                        <Text style={styles.TextBold}>ID nieruchomości:</Text><Text style={styles.Text}> {this.data.key}</Text>
                    </View>
                    {this.LotSize()}
                    {this.FlatSize()}
                    {this.property_rooms()}
                    {this.property_bedrooms()}
                    {this.property_bathrooms()}

                </View>
                {/* {this.features()} */}
                <Text style={{ fontSize: 25, fontFamily:"Nunito-Bold", marginTop: 10 }}>Detale</Text>
                <View style={{ width: 40, height: 5, backgroundColor: '#f6921e', marginTop: 5, marginBottom: 5 }} />

                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    alignItems: 'flex-start' // if you want to fill rows left to right
                }}>
                    {this.property_more(this.data.property.balkontaras, 'balkon/taras')}
                    {this.property_more(this.data.property.basen, 'basen')}
                    {this.property_more(this.data.property.do500modplazy, '500 do plaży')}
                    {this.property_more(this.data.property.do1kmodplazy, '1km do plaży')}
                    {this.property_more(this.data.property.garaz, 'garaż')}
                    {this.property_more(this.data.property.klimatyzacja, 'klimatyzacja')}
                    {this.property_more(this.data.property.miejsceparkingowe, 'parking')}
                    {this.property_more(this.data.property.ogrodek, 'ogród')}
                    {this.property_more(this.data.property.pierwszalinia_brzegowa, 'linia brzegowa')}
                    {this.property_more(this.data.property.przypolachgolfowych, 'przy polach golfowych')}
                    {this.property_more(this.data.property.umeblowane, 'umeblowanie')}
                    {this.property_more(this.data.property.widoknamorze, 'widok na morze')}
                    {this.property_more(this.data.property.winda, 'winda')}
                </View>
            </View>
        );
    }

}
export { Details };