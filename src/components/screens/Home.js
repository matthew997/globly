import React from 'react';
import {createAppContainer} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {LogoTitle,MenuIcon} from '../../components/Helper/Header';
import {DrawerNavigator} from '../../components/Helper/Router';

const AppNavigator = createStackNavigator({
  DrawerNavigator:{screen:DrawerNavigator,
    navigationOptions: ({ navigation }) => ({
      headerRight: <MenuIcon navig={navigation}/>,
      headerTitle: <LogoTitle/>,
      headerTintColor: 'white',
      headerStyle: {
        backgroundColor: '#FFEC51',
        borderBottomColor: '#FFEC51',
      },
    }),
  }
});

const Home = createAppContainer(AppNavigator);
export {Home};
