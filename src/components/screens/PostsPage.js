import React, { Component } from 'react';
import {
    Text,
    View,
    FlatList,
    StyleSheet,
    ActivityIndicator,
    Dimensions,
    Image,
    TouchableOpacity,
    TextInput, Alert,
    StatusBar,BackHandler
} from 'react-native';
import { getData } from '../Helper/Storage';
var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: 'UserDatabase.db' });
var width = Dimensions.get('window').width;
export default class PostsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            favourite: [],
            page: 1,
            helper: 1,
            rangeLow: '',
            rangeHigh: '',
            order: 'ASC',
            error: "",
            nickname: '',
        };
        this.featchDataPrice();
    }
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
      }
    
      handleBackPress = () => {
        this.props.navigation.navigate('Home');
        return true; 
      }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        getData('@nickname').then(result => { this.setState({ nickname: result }) });
        this.subs = this.props.navigation.addListener("didFocus", () => {
            this.LikeStorage()
        });
    }
    LikeStorage() {
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM Favourites', [], (tx, results) => {
                var temp = [];
                for (let i = 0; i < results.rows.length; ++i) {
                    temp.push(results.rows.item(i)['postId']);
                }
                this.setState({
                    favourite: temp,
                });

            });
        });
    }
    async featchData() {
        const response = await fetch(
            `https://globly.eu/wp-json/wp/v2/apartments/search?order=${this.state.order}&price_min=${this.state.rangeLow}&price_max=${this.state.rangeHigh}&page=${this.state.page}`
        );
        const json = await response.json();
        this.setState(state => ({
            data: [...state.data, ...json],
            loading: false
        }));
    };
    async featchDataPrice() {
        const response = await fetch(
            `https://globly.eu/wp-json/wp/v2/apartments/search?order=${this.state.order}&price_min=${this.state.rangeLow}&price_max=${this.state.rangeHigh}&page=${this.state.page}`
        );
        const json = await response.json();
        if (json.length) {
            this.setState({
                data: json,
                loading: false,
                page: 1,
            });
        } else {
            this.setState({
                rangeLow: '',
                rangeHigh: '',
                loading: true
            })
            this.featchData()
            Alert.alert('', 'Brak wyników wyszukiwania');

        }


    };
    handleEnd = () => {
        this.setState({ page: this.state.page + 1 }, () => this.featchData());

    }
    render() {
        let { rangeLow, rangeHigh } = this.state;
        return (
            <View style={styles.container}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor="#fff267"
                />
                <Text style={{ textAlign: 'center', marginTop: 7, fontFamily: 'Nunito-Bold' }}>¡Hola {this.state.nickname}! Odkryj najnowsze oferty!</Text>
                <View style={{
                    // flex: 1,
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    alignItems: 'flex-start' // if you want to fill rows left to right
                }}>
                    <View style={{ flexDirection: 'row', width: '50%' }}>
                        <TextInput
                            placeholder="Cena minimalna w €"
                            placeholderTextColor="#727272"
                            value={rangeLow}
                            onChangeText={(rangeLow) => {
                                this.setState({ rangeLow, helper: 0, data: [], page: 1 });
                            }}
                            onEndEditing={() => {
                                this.setState({ data: [], page: 1 });
                                this.featchDataPrice()
                            }}
                            keyboardType={'numeric'}
                            style={[styles.textInput]}

                        />
                    </View>
                    <View style={{ flexDirection: 'row', width: '50%' }}>
                        <TextInput
                            placeholder="Cena maksymalna w €"
                            placeholderTextColor="#727272"
                            value={rangeHigh}
                            onChangeText={(rangeHigh) => {
                                this.setState({ rangeHigh, helper: 0, data: [], page: 1 });
                            }}
                            onEndEditing={() => {
                                this.setState({ data: [], page: 1 });
                                this.featchDataPrice()
                            }}
                            keyboardType={'numeric'}
                            style={[styles.textInput]}
                        />
                    </View>
                </View>

                <FlatList
                    data={this.state.data}
                    keyExtractor={item => item.slug}
                    onEndReached={() => this.handleEnd()}
                    initialNumToRender={10}
                    onEndReachedThreshold={0.5}
                    ListFooterComponent={() =>
                        this.state.loading
                            ? null
                            : <ActivityIndicator size="large" animating color="#fff267" />}
                    renderItem={({ item }) => <Item post={item} favourite={this.state.favourite} nav={this.props.navigation} />}
                />

            </View>
        );
    }
}
class Item extends Component {
    numberWithCommas(x) {
        if (x === 0) {
            return this.props.post.price_before;
        } else {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' €';
        }
    }
    titleShorter(x) {
        if ((x).length > 50) {
            return (((x).substring(0, 47)) + '...');
        } else {
            return x;
        }
    }
    compare(id) {
        let data2 = this.props.favourite.filter(function (e) {
            return e == id;
        });
        if (data2[0]) {
            return 1;
        } else {
            return 0;
        }
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white', marginBottom: 10, paddingLeft: 7, paddingBottom: 10 }}>

                <View style={styles.image}>
                    <View style={{ position: 'absolute', width: 30, height: 30, marginLeft: 75, marginTop: 5, zIndex: 999 }}>
                        {this.compare(this.props.post.id) == 1 ? <Image
                            source={require('../../images/like-2.png')}
                            style={{ width: 20, height: 20 }} /> : null}

                    </View>
                    <Image
                        source={{ uri: this.props.post.featured_image.thumbnail }}
                        style={{ width: 100, height: 100, borderBottomLeftRadius: 7, borderTopLeftRadius: 7 }} />
                </View>
                <View style={{ position: 'absolute', marginLeft: 110 }}>
                    <Text style={styles.item}>{this.titleShorter(this.props.post.title)}</Text>
                    <Text style={styles.text2}>{this.numberWithCommas(this.props.post.price)}</Text>
                    <TouchableOpacity style={{
                        ...styles.button,
                        backgroundColor: '#f6921e',
                    }}
                        onPress={() => this.props.nav.navigate('Single', { id: this.props.post.id })
                        }>
                        <Text style={{ fontSize: 17, fontWeight: 'bold', color: 'white', fontFamily: "Nunito-Light", marginTop: -3 }}>
                            Odkryj
                    </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}


export { PostsPage };
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 0
    },
    textInput: {
        width: width / 2 - 20,
        color: '#727272',
        backgroundColor: "#fff267",
        height: 50,
        borderRadius: 25,
        // borderWidth: 0.6,
        marginHorizontal: 10,
        paddingLeft: 10,
        marginVertical: 10,
        borderColor: 'rgba(0,0,0,0.3)',
        zIndex: 1,
        alignSelf: 'center'
    },
    item: {
        // textAlign: 'justify',
        fontFamily: "Nunito-Bold",
        // fontWeight: 'bold',
        padding: 9,
        paddingRight: 10,
        fontSize: 16,
        width: width - 110,


    },
    text2: {
        marginTop: 10,
        marginLeft: 15,
        fontSize: 20,
        // fontWeight: 'bold',
        color: 'black',
        backgroundColor: '#fff278',
        alignSelf: 'flex-start',
        padding: 5,
        fontFamily: "Nunito-Bold"
    },
    button: {
        // position:'relative',
        marginLeft: width - 225,
        marginTop: -33,
        backgroundColor: 'white',
        height: 30,
        width: 100,
        marginHorizontal: 20,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 5
    },
    image: {
        width: 100,
        height: 100,
        position: 'relative',
        marginTop: 10,
        flexDirection: 'row',
    }
})

