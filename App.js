import React, { Component } from 'react';

import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from 'react-navigation';
import { View, Dimensions, Image,ActivityIndicator } from 'react-native';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

import { LogoTitle, MenuIcon } from './src/components/Helper/Header';
import { Login } from './src/components/screens/Login';
import { Register } from './src/components/screens/Register';
import { Home } from './src/components/screens/Home';
import { NoLogin } from './src/components/screens/NoLogin';
import { Single } from './src/components/screens/Single';
import { getData, storeData } from './src/components/Helper/Storage';
var SQLite = require('react-native-sqlite-storage')
var db = SQLite.openDatabase({ name: 'UserDatabase.db' });

console.disableYellowBox = true;
class AutoLogin extends Component {
  constructor() {
    super();
    db.transaction(function(txn) {
      txn.executeSql(
        'CREATE TABLE IF NOT EXISTS Favourites(id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), postId INT(10))',
        []
      );
    });
    storeData('@nickname', 'Globlito');
    getData('token').then(result => this.isLogin(result));
  }

  isLogin(token) {
    let loginPermition = false;
   
    fetch('https://globly.eu/api/user/get_user_meta/?cookie=' + token)
      .then((response) => response.json())
      .then((json) => {
        if (json.status == "ok") {
          loginPermition = true;

          storeData('@nickname', json['nickname']);
      
          this.props.navigation.navigate("Home");
        } else {
          loginPermition = false;
          this.props.navigation.navigate("NoLogin");
        }
      })
    // setTimeout(() => {
    //   this.props.navigation.navigate(loginPermition ? "Home" : "NoLogin");
    // }, 5000);
  }
  render() {

    return (
      <View>
        <View style={{
          justifyContent: 'center',
          alignItems: 'center',
          flex: 1,
          marginTop: height / 2-50,
        }}>
          <Image source={require('./src/images/logo.png')} style={{ width: 250, height: 250 }} />
          <ActivityIndicator size="large" color="#fff267" style={{marginTop:30}} />
        </View>
      </View>
    )
  }
}


const AppNavigatorr = createStackNavigator({
  AutoLogin: {
    screen: AutoLogin,
    navigationOptions: {
      header: null,
    }
  },
  Home: {
    screen: Home,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  NoLogin: {
    screen: NoLogin,
    navigationOptions: {
      header: null,
      gesturesEnabled: false
    }
  },
  Login: {
    screen: Login,
  },
  Register: {
    screen: Register,
  },
  Single: {
    screen: Single,
    navigationOptions: ({ navigation }) => ({
      // headerRight: <MenuIcon navig={navigation} />,
      headerTitle: <LogoTitle />,
      headerTintColor: 'white',
      headerStyle: {
        backgroundColor: '#FFEC51',
        borderBottomColor: '#FFEC51',
      }
    })
  }
}, {
  initialRouteName: 'AutoLogin'
});
const App = createAppContainer(AppNavigatorr);
export default App;
